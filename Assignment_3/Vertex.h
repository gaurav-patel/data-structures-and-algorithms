/*
Author:		Gaurav Patel
Date:		August 21, 2015
Purpose:	Assignment 3 - Skeleton of Vertex class it holds information on jobs and setters and getters functions to
			set and get data to/from Vertex(Job) object.
*/
#pragma once

#include <iostream>
#include <string>
using namespace std;

#include "PreJob.h"

class Vertex
{
	int time;
	string id;
	int start_time;
	int length;
	bool processed;
	PreJob* prejob; //Points to job which must be done before this is processed.

	public:
		Vertex(int t, string i, int s, int l, PreJob* p);
		int getTime();
		string getId();
		int getStartTime();
		int getLength();
		void setStartTime(int s);
		void setLength(int l);
		bool isPocessed();
		void setProcessed(bool p);
		PreJob* getPreJob();
		bool isEmpty();
		void print(ostream& os); //used ostream so I can print data to screen and also write to file.
};