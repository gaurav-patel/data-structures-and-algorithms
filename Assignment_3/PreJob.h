/*
Author:		Gaurav Patel
Date:		August 21, 2015
Purpose:	Assignment 3 - This class holds information on ID of job and prejob points to next prority job if there is any.
*/
#pragma once

class PreJob
{
	string prejobID;
	PreJob* prejob;

	public:
		PreJob(string id, PreJob* next)
		{
			prejobID = id;
			prejob = next;
		}
		string getID()
		{
			return prejobID;
		}
		PreJob* getNext()
		{
			return prejob;
		}
};