/*
Author:		Gaurav Patel
Date:		August 21, 2015
Purpose:	Assignment 3 - Implementation of Graph class. It creates graph from file received in constructor using that file
			it creates vertices and edges(prejob). The jobs are processed in time as incremental variable and using job data 
			from vertices using it's start time and duration of job. Finally print function which receives a output stream 
			object in which all jobs is sent to output stream.
*/

#include "Graph.h"

Graph::Graph(string filename)
{
	for (int i = 0; i < MAX; i++)
		source[i] = nullptr;
	index = 0;

	string line;
	int time, start, length;
	string id, prejob;
	ifstream filestream(filename);
	if (filestream.is_open())
	{
		while (getline(filestream, line))
		{
			time = start = length = 0;
			id = prejob = "none";

			time = atoi(line.substr(0, line.find(' ')).c_str());
			line = line.erase(0, line.find(' ') + 1);
			if (line.size() > 3)
			{
				id = line.substr(0, line.find(' '));
				line = line.erase(0, line.find(' ') + 1);
				
				if (id.compare("critical") != 0)
				{
					start = atoi(line.substr(0, line.find(' ')).c_str());
					line = line.erase(0, line.find(' ') + 1);

					length = atoi(line.substr(0, line.find(' ')).c_str());
					line = line.erase(0, line.find(' ') + 1);

					if (line.size() > 1)
					{
						prejob = line;
					}
				}
				
			}
			AddVertex(time, id, start, length, prejob);
		}
		filestream.close();
	}
	else
	{
		cerr << "Unable to open " << filename << " file." << endl;
	}

}

void Graph::AddVertex(int t, string i, int s, int l, string prejob)
{
	PreJob* p = nullptr;
	if (prejob.compare("none") != 0)
	{
		p = new PreJob(prejob, nullptr);
	}
	
	if (index < MAX)
	{
		source[index++] = new Vertex(t, i, s, l, p);
	}
	else
	{
		cerr << "No more space for jobs." << endl;
	}
}

int Graph::findIndexOfJob(string jobId)
{
	for (int i = 0; i < index; i++)
	{
		if (jobId.compare(source[i]->getId()) == 0)
		{
			return i;
		}
	}
	return -1;
}

void Graph::processJobs()
{
	int t = 1;
	int traverse = 0;
	bool finished = false;

	while (finished == false)
	{
		if (source[traverse]->getId().compare("critical") == 0)
		{
			++traverse;
			--t; //when critical is found, the time doesn't suppose to increment thats why I decremented it so when 
				//it incrementes later in this func it will be at it's original time.
		}

		if (t >= source[traverse]->getStartTime())
		{
			PreJob* pre = source[traverse]->getPreJob();
			if (pre != nullptr)
			{
				t = processRecursive(t, pre);
				t = processsVertex(t, traverse);
				++traverse;
			}
			else
			{
				t = processsVertex(t, traverse);
				++traverse;
			}
		}

		++t;

		if (traverse >= index)
		{
			finished = true;
		}
	}
}

int Graph::processRecursive(int currTime, PreJob* p)
{
	string id = p->getID();
	int index = findIndexOfJob(id);

	if (!source[index]->isPocessed())
	{
		PreJob* n = source[index]->getPreJob();
		if (n != nullptr)
			currTime += processRecursive(currTime, p);
		else
		{
			currTime += processsVertex(currTime, index);
		}

	}

return currTime;
}

int Graph::processsVertex(int currTime, int vertexToProcess)
{
	bool empty = source[vertexToProcess]->isEmpty();
	
	if (empty == false)
	{
		source[vertexToProcess]->setProcessed(true);
		int temp = currTime + source[vertexToProcess]->getLength();
		source[vertexToProcess]->setLength(temp);
		source[vertexToProcess]->setStartTime(currTime);
		return temp;
	}

return currTime;
}

void Graph::print(ostream& os)
{
	for (int i = 0; i < index; i++)
	{
		string s = source[i]->getId();
		if (s.compare("critical") == 0)
		{
			string n = source[i - 1]->getId();
			if (n.compare("critical") == 0)
			{
				os << i + 1 << " ";
				source[i - 2]->print(os);
				os << endl;
			}
			else
			{
				os << i + 1 << " ";

				PreJob* p = source[i - 1]->getPreJob();
				while (p != nullptr)
				{
					string id = p->getID();
					int index = findIndexOfJob(id);
					source[index]->print(os);
					p = p->getNext();
				}

				source[i - 1]->print(os);
				os << endl;
			}
		}
	}
}
