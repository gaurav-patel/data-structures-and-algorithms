/*
Author:		Gaurav Patel
Date:		August 21, 2015
Purpose:	Assignment 3 - Skeleton for Jobs graph, processing jobs on graph, and outputing graph.
*/
#pragma once

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include "Vertex.h"
#define MAX 10

class Graph
{
	Vertex* source[MAX];
	int index;
	
	public:
		Graph(string filename);
		void AddVertex(int t, string i, int s, int l, string prejob);
		int findIndexOfJob(string jobId);
		void processJobs();
		int processRecursive(int currTime, PreJob* p);
		int processsVertex(int currTime, int vertexToProcess);
		void print(ostream& os); //used ostream so I can print data to screen and also write to file.
};

