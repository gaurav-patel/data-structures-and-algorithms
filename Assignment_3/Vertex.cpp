/*
Author:		Gaurav Patel
Date:		August 21, 2015
Purpose:	Assignment 3 - Implementation of Vertex(Job) class which holds information on jobs and some getters and setters to
			get and set data and empty checking function. A PreJob object holds PreJob that has higher priority before current 
			job. Also used print function which receives a output stream object in which all jobs is sent to output stream.
*/

#include "Vertex.h"

Vertex::Vertex(int t, string i, int s, int l, PreJob* p)
{
	id = i;
	time = t;
	start_time = s;
	length = l;
	prejob = p;
	processed = false;
}

int Vertex::getTime()
{
	return time;
}

string Vertex::getId()
{
	return id;
}

int Vertex::getStartTime()
{
	return start_time;
}

int Vertex::getLength()
{
	return length;
}

void Vertex::setStartTime(int s)
{
	if (s > 0)
		start_time = s;
}

void Vertex::setLength(int l)
{
	if (l > 0)
		length = l;
}

void Vertex::setProcessed(bool p)
{
	processed = p;
}

bool Vertex::isPocessed()
{
	return processed;
}

PreJob* Vertex::getPreJob()
{
	return prejob;
}

bool Vertex::isEmpty()
{
	if (id.compare("none") == 0 || id.compare("critical") == 0)
		return true;
	else
		return false;
}

void Vertex::print(ostream& os)
{
	if (id.compare("none") != 0)
		os << id << " " << start_time << " " << length << " ";
}
