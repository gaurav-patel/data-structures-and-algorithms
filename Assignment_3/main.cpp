/*
Author:		Gaurav Patel
Date:		August 21, 2015
Purpose:	Assignment 3 - Creates a graph from file, process jobs retived from file, and outputs the final result
			to output.txt file and screen.
*/

#include <iostream>
using namespace std;
#define FILE "jobs.txt"
#define OUTFILE "output.txt"

#include "Graph.h"

int main(int argc, char** args)
{
	Graph g = Graph(FILE);

	g.processJobs();

	ofstream output;
	output.open(OUTFILE);
	g.print(output);
	output.close();

	g.print(cout);

return 0;
}