# README #

This repository contains assignments I've did in Data Structure and Algorithms course.

### Assignment 1 ###

Assignment 1 is about accepting polynomial problems and solving them, it makes use of Circular List node structure.

### Assignment 2 ###

Assignment 2 is a game where user guesses an animal and the program tries to identify the guess by asking questions. It uses binary tree structure which helps eliminate half of the other choices.

### Assignment 3 ###

Assignment 3 is using graph data structure to identify and create print jobs queue.