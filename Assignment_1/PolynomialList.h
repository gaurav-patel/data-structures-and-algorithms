/*
Author:		Gaurav Patel
Date:		June 6, 2015
Purpose:	Assignment 1 - Skeleton for storing Polynomial problem which is stored in List of Nodes
			until a specified operation is performed provided by professor.
*/

#pragma once;

#include<iostream>
#include<string>
#include<sstream>
#include<set>
using namespace std;

class Node {
	friend class Polynomial; //So Polynomial class can object private fields of Node class.

	int coefficient;
	int exponent;
	Node* next;

public:
	Node();
	Node(int c, int e);
	void setNext(Node* n);
	Node* getNext();
	int get_coefficient();
	int get_exponent();
};

class Polynomial {
	Node* first;
	Node* last;

public:
	Polynomial();
	~Polynomial();
	friend istream& operator>>(istream& insert, Polynomial& poly);
	friend ostream& operator<<(ostream& output, const Polynomial& poly);
	friend Polynomial& operator+(const Polynomial& poly1, const Polynomial& poly2);
	friend Polynomial& operator-(const Polynomial& poly1, const Polynomial& poly2);
	friend Polynomial& operator*(const Polynomial& poly1, const Polynomial& poly2);
};