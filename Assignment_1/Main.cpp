/*
Author:		Gaurav Patel
Date:		June 6, 2015
Purpose:	Assignment 1 - Main function, displaying Polynomial calculation options and redirecting user to 
			apporiate functions.

*/
#include<iostream>
using namespace std;

#include "PolynomialList.h";

int main()
{
	int choice;
	Polynomial poly1;
	Polynomial poly2;
	Polynomial answer;

	do
	{
		cout << "Polynomial Calculator\n"
			<< "1. Add two Polynomials\n"
			<< "2. Substract two Polynomials\n"
			<< "3. Multiply Polynomials\n"
			<< "4. Exit\n";

		cout << "Enter your choice: ";
		cin >> choice;
		cin.ignore();
		
		if (choice < 4 && choice > 0)
		{
			cout << "Insert only coefficients and exponents (Enter Zero if no exponent)";
			cout << "seperated by space and ending with 0 -1 in descending order using exponent.\n";

			cout << "Insert first polynomial: ";
			cin >> poly1;

			cout << "Insert second polynomial: ";
			cin >> poly2;
		}

		switch (choice)
		{
			case 1:
				answer = poly1 + poly2;
				cout << "Answer: " << answer;
				
				poly1.~Polynomial();
				poly2.~Polynomial();
				answer.~Polynomial();

				break;

			case 2:
				answer = poly1 - poly2;
				cout << "Answer: " << poly1 - poly2;
				
				poly1.~Polynomial();
				poly2.~Polynomial();
				answer.~Polynomial();

				break;

			case 3:
				answer = poly1 * poly2;
				cout << "Answer: " << answer;

				poly1.~Polynomial();
				poly2.~Polynomial();
				answer.~Polynomial();

				break;
		}
	} while (choice != 4);

	system("pause");
return 0;
}