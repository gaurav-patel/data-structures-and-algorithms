/*
Author:		Gaurav Patel
Date:		June 6, 2015
Purpose:	Assignment 1 - Implementation of Node class and Polynomial class to	do simple 
			math calculation on Polynomial problems. Each calculating field in Polynomial
			is stored in a Node so it can handle all sizes of Polynomial problems.
*/

#include "PolynomialList.h";

//------------------------------------- Node class Methods -----------------------------------
//Node object default constructor, sets coefficient and exponent values to 0 & -1 to give node with
// default values and when looping through the loop we can say these are empty objects.
Node::Node()
{
	coefficient = 0;
	exponent = -1;
	next = nullptr;
}
//Node object override constructor, creates an object with passed in values.
Node::Node(int coeff, int expo)
{
	coefficient = coeff;
	exponent = expo;
	next = nullptr;
}
//Sets value of *next variable.
void Node::setNext(Node* n)
{
	next = n;
}
//returns address/value of *next variable.
Node* Node::getNext()
{
	return next;
}
//returns coefficient of the current Node.
int Node::get_coefficient()
{
	return coefficient;
}
//return exponent of the current Node.
int Node::get_exponent()
{
	return exponent;
}

//--------------------------------------- Polynomial class Methods ----------------------------

//Default Constructor: Creates a Node object and addes it to Polynomial problem to know starting
//and ending point in circular linked list.
Polynomial::Polynomial()
{
	Node* temp = new Node(0, -1);
	temp->setNext(temp);

	first = temp;
	last = temp;
}

//Destructor: looping through the linked list and deleting all the Nodes.
Polynomial::~Polynomial()
{
	Node* temp = first->getNext();
	Node* next = temp->getNext();

	while (temp != first)
	{
		delete temp;
		temp = next;
		if (next->getNext())
			next = next->getNext();
	}
	temp = first;
	temp->setNext(temp);
	first = last = temp;
}

//Overloaded inputstream accepts user's enter values and creates a Polynomial linked list object.  
istream& operator>>(istream& insert, Polynomial& poly)
{	
	int c = 0, e = 0;
	Node* temp; 
	string line;
	
	getline(insert, line); 
	
	if (line.find("0 -1") != -1)
	{
		istringstream ss(line);
		string word;
		
		do{
			ss >> word;
			c = stoi(word);
			ss >> word;
			e = stoi(word);
			
			if (c != 0 && e != -1)
			{
				temp = new Node(c, e);
				temp->setNext(poly.first);

				poly.last->setNext(temp);
				poly.last = temp;
			}
		} while (c != 0 && e != -1); 
	}
return insert;
} //End of istream is >> Polynomial.

//Overloaded outputstream which prints out passed in Polynomial in specific way. 
//Used design is: #X^# & repeate until end of linked list. 
ostream& operator<<(ostream& output, const Polynomial& poly)
{
	Node* temp = poly.first->getNext();

	int coff, expo;
	int itr = 1;

	while (temp != poly.first)
	{
		coff = temp->get_coefficient();
		expo = temp->get_exponent();

		if (itr > 1 && coff >= 0)
			output << "+" << coff;
		else
			output << coff;

		if (expo == 1)
			output << "X";
		else if (expo != 0)
			output << "X^" << expo;

		temp = temp->getNext();
		++itr;
	}

	output << "\n" << endl;

return output;
} //END of ostream& os << Polynomial.

//Overloaded operator + which adds two passed in Polynomials and returns the result in Polynomial object.
//Big O is (N^2).
Polynomial& operator+(const Polynomial& poly1, const Polynomial& poly2)
{
	Polynomial* result = new Polynomial();

	if (poly1.first == poly1.last || poly2.first == poly2.last)
	{
		return *result;
	}

	Node* one = poly1.first->getNext();
	Node* two = poly2.first->getNext();

	Node* temp;
	int expo_one, coeff_one, expo_two, coeff_two;
	//Big O for everything before this: O(12)
	
	do {
		if (one == poly1.first)
		{
			do{
				temp = new Node(two->get_coefficient(), two->get_exponent());
				
				temp->setNext(result->first);

				result->last->setNext(temp);
				result->last = temp;

				two = two->getNext(); //Moving Poly2 to next Node.
			
			} while (two != poly2.first);
		return *result;
		}//Big O of if including checking if condition: 1 + O(7) * N
		else if (two == poly2.first)
		{
			do{
				temp = new Node(one->get_coefficient(), one->get_exponent());
				
				temp->setNext(result->first);

				result->last->setNext(temp);
				result->last = temp;

				one = one->getNext(); //Moving Poly2 to next Node.

			} while (one != poly1.first);
		return *result;
		}//Big O of else if including checking if condition: 1 + O(7) * N

		expo_one = one->get_exponent();
		coeff_one = one->get_coefficient();
		expo_two = two->get_exponent();
		coeff_two = two->get_coefficient();
		//Big O: O(8)

		if (expo_one > expo_two)
		{
			temp = new Node(coeff_one, expo_one);

			temp->setNext(result->first);

			result->last->setNext(temp);
			result->last = temp;

			one = one->getNext(); //Moving Poly1 to next Node.
		}//Big O of if including checking if condition: 1 + O(7)
		else if (expo_two > expo_one)
		{
			temp = new Node(coeff_two, expo_two);

			temp->setNext(result->first);

			result->last->setNext(temp);
			result->last = temp;

			two = two->getNext(); //Moving Poly2 to next Node.
		}//Big O of else if including checking else if condition: 1 + O(7)
		else
		{
			int coeff = coeff_one + coeff_two;
			if (coeff != 0)
			{
				temp = new Node(coeff, expo_one);

				temp->setNext(result->first);

				result->last->setNext(temp);
				result->last = temp;
			}

			one = one->getNext(); //Moving Poly1 to next Node.
			two = two->getNext(); //Moving Poly2 to next Node.
		}//Big O of else: O(12)
	} while (one != poly1.first || two != poly2.first); //Big of of condition: N * N = N^2

	//Big O of While condition with inside statements: O(12) + [ O(4) + O(8) + [O(7) * N] + O(12) ] * N^2
	//O(12): Declararation + 2 if check statement on top. O(4): if check conditions in main/big do-while loop.
	//O(8): assigns variable + get method for assigning variable functions. 
	//O(7) * N: do-while condition for loop in if statement in beginning of main/big do-while loop.
	//O(12): biggest time from one of the if-else if-else statement.
	//Final Big O: N^2 because all the inner loops/other constants are out and O(7)*N is one of the if statement so it will not be worst case.
return *result;
} //End of operator+: Polynomial + Polynomial.

//Overloaded operator - which substracts from passed in first Polynomial to second one and returns the result in Polynomial.
//Big O is (N^2).
Polynomial& operator-(const Polynomial& poly1, const Polynomial& poly2)
{
	Polynomial* result = new Polynomial();

	if (poly1.first == poly1.last || poly2.first == poly2.last)
	{
		return *result;
	}

	Node* one = poly1.first->getNext();
	Node* two = poly2.first->getNext();

	Node* temp;
	int expo_one, coeff_one, expo_two, coeff_two;
	//Big O for everything before this: O(12)

	do {
		if (one == poly1.last->getNext())
		{
			do{
				expo_two = two->get_exponent();
				coeff_two = -1 * two->get_coefficient();

				temp = new Node(coeff_two, expo_two);
				temp->setNext(result->first);

				result->last->setNext(temp);
				result->last = temp;

				two = two->getNext(); //Moving Poly2 to next Node.

			} while (two != poly2.last->getNext());
		return *result;
		}//Big O of if including checking if condition: 2 + O(11) * N
		else if (two == poly2.last->getNext())
		{
			do{
				expo_one = one->get_exponent();
				coeff_one = one->get_coefficient();

				temp = new Node(coeff_one, expo_one);
				temp->setNext(result->first);

				result->last->setNext(temp);
				result->last = temp;

				one = one->getNext(); //Moving Poly2 to next Node.

			} while (one != poly1.last->getNext());
		return *result;
		}//Big O of if including checking if condition: 2 + O(11) * N

		expo_one = one->get_exponent();
		coeff_one = one->get_coefficient();
		expo_two = two->get_exponent();
		coeff_two = -1 * two->get_coefficient();
		//Big O: O(9)

		if (expo_one > expo_two)
		{
			temp = new Node(coeff_one, expo_one);

			temp->setNext(result->first);

			result->last->setNext(temp);
			result->last = temp;

			one = one->getNext(); //Moving Poly1 to next Node.
		}//Big O of if including checking if condition: 1 + O(7)
		else if (expo_two > expo_one)
		{
			temp = new Node(coeff_two, expo_two);

			temp->setNext(result->first);

			result->last->setNext(temp);
			result->last = temp;

			two = two->getNext(); //Moving Poly2 to next Node.
		}//Big O of if including checking if condition: 1 + O(7)
		else
		{
			int coeff = coeff_one + coeff_two;

			if (coeff != 0)
			{
				temp = new Node(coeff, expo_one);

				temp->setNext(result->first);

				result->last->setNext(temp);
				result->last = temp;
			}

			one = one->getNext(); //Moving Poly1 to next Node.
			two = two->getNext(); //Moving Poly2 to next Node.
		}//Big O of if including checking if condition: O(12)
	} while (one != poly1.last->getNext() || two != poly2.last->getNext());//Big of of condition: N * N = N^2

	//Big O of While condition with inside statements: O(12) + [ O(4) + O(8) + [O(7) * N] + O(12) ] * N^2
	//O(12): Declararation + 2 if check statement on top. O(4): if check conditions in main/big do-while loop.
	//O(8): assigns variable + get method for assigning variable functions. 
	//O(7) * N: do-while condition for loop in if statement in beginning of main/big do-while loop.
	//O(12): biggest time from one of the if-else if-else statement.
	//Final Big O: N^2 because all the inner loops/other constants are out and O(7)*N is one of the if statement so it will not be worst case.

return *result;
} //End of operator-: Polynomial - Polynomial.

//Override operator * multiplies two Polynomials and returns result in Polynomial object.
//Big O is O(N^4).
Polynomial& operator*(const Polynomial& poly1, const Polynomial& poly2)
{
	Polynomial* result = new Polynomial();

	if (poly1.first == poly1.last || poly2.first == poly1.last)
	{
		return *result;
	}

	Node* one = poly1.first->getNext();
	Node* two = poly2.first->getNext();
	Node* temp;

	int coeff, expo, coeff_one, expo_one;
	//Big O for everything before this: O(12)

	do{
		coeff_one = one->get_coefficient();
		expo_one = one->get_exponent();

		do{
			coeff = coeff_one * two->get_coefficient();
			expo = expo_one + two->get_exponent();

			temp = new Node(coeff, expo);
			temp->setNext(result->first);

			result->last->setNext(temp);
			result->last = temp;

			two = two->getNext();
			//Big O of constants inside do-while: O(13)
		} while (two != poly2.last->getNext());//Big O of do-while condition: N, and for the loop is: O(13) * N

		one = one->getNext();
		two = poly2.first->getNext();
		//Big O of insode the loop: O(8)  
	} while (one != poly1.last->getNext());//Big O of do-while condition: N, and for the loop is: O(8) + [O(13)* N] * N. 

	Polynomial* final = new Polynomial();
	one = result->first->getNext();
	Node* add;

	set<int> myset;
	//Big O: O(5)

	do{
		coeff = one->get_coefficient();
		expo = one->get_exponent();
		two = result->first->getNext();
		//Big O: O(6)
		do{
			expo_one = two->get_exponent();
			
			if (myset.count(expo_one) == 0)
			{
				if (expo == expo_one && one != two)
				{
					coeff = coeff + two->get_coefficient();
				}
			}

			two = two->getNext();
			//Big O: O(11)
		} while (two != result->last->getNext()); //Big O of do-while condition: N, and for the loop is: O(11) * N.

		if (myset.count(expo) == 0)
		{
			myset.insert(expo);
			add = new Node(coeff, expo);

			add->setNext(final->first);

			final->last->setNext(add);
			final->last = add;
		}//Big O: O(8)

		one = one->getNext();
		//Big O: O(2)
	} while (one != result->last->getNext());//Big O of do-while condition: N, and for the loop is: [O(8) + O(2) + O(11)*N + O(6)] * N
	//Big O of first do-while{do-while{}}-> loop with in loop: O(8) + [O(13)* N] * N === N * N === N^2
	//Big O of the second do-while{do-while{}}-> loop with in loop: [O(8) + O(2) + O(11)*N + O(6)] * N === N * N === N^2

return *final;

} //End of operator*: Polynomial * Polynomial.