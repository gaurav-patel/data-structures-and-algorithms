/*
Author:		Gaurav Patel
Date:		July 10, 2015
Purpose:	Assignment 2 - Binary_tree_Node class is a part of Binary tree class. Binary tree class holds Binary tree node
			object as a root and binary tree node class and has specified data type held in binary tree class to hold 
			information and two Binary tree node type/class objects to pointers to. In this way a Binary tree Node class 
			can point to other Binary tree node objects and they can point to others, which makes tree like structre.
*/

#pragma once

#include <iostream>
#include <memory>
using namespace std;

template <class Item>
class Binary_tree_Node
{
	Item info;
	shared_ptr<Binary_tree_Node> right;
	shared_ptr<Binary_tree_Node> left;

public:
	Binary_tree_Node()
	{
		right = nullptr;
		left = nullptr;
	}

	Binary_tree_Node(Item data)
	{
		info = data;
		right = nullptr;
		left = nullptr;
	}

	const Item& data() const
	{
		return info;
	}

	void set_data(const Item& new_data)
	{
		info = new_data;
	}

	void set_left(shared_ptr<Binary_tree_Node> new_link)
	{
		if (new_link != nullptr)
		{
			this->left = new_link;
		}
	}

	void set_right(shared_ptr<Binary_tree_Node> new_link)
	{
		if (new_link != nullptr)
		{
			this->right = new_link;
		}
	}

	shared_ptr<Binary_tree_Node> get_left()
	{
		return this->left;
	}

	shared_ptr<Binary_tree_Node> get_right()
	{
		return this->right;
	}

	bool is_leaf()
	{
		if (this->left == nullptr && this->right == nullptr)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
};