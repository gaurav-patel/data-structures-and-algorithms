/*
Author:		Gaurav Patel
Date:		July 10, 2015
Purpose:	Assignment 2 - Animal guessing game main implementation. A Binary tree object is created and used to
			get questions to show user to narrow down and guess user's guessed animal.
*/


#include <iostream>
using namespace std;
#include <string>

#include "BinaryTree.h"

#define FILE_NAME "HeapQuestions.txt"

template <class Item>
void play_guess_game(Binary_tree<Item>& tree, shared_ptr<Binary_tree_Node<string>>& current)
{
	string user_input;
	bool keep_going = true;

	while (keep_going)
	{
		bool leaf = current->is_leaf();
		if (leaf)
		{
			cout << "Are you a " << current->data() << "?" << endl;
		}
		else
		{
			cout << "Q: " << current->data() << endl;
		}
		cout << "A: ";
		cin >> user_input;

		if (user_input[0] != 'Y' && user_input[0] != 'N' && user_input[0] != 'y' && user_input[0] != 'n')
		{
			cout << "\nUnknown Input, Please answer question again." << endl;
		}
		else
		{
			//Checking if leaf is true and user guess is correct.
			if (leaf && (user_input[0] == 'Y' || user_input[0] == 'y'))
			{
				cout << "\nI knew it!" << endl;
				keep_going = false;
				break;
			}

			if (!leaf && (user_input[0] == 'Y' || user_input[0] == 'y'))
			{
				current = current->get_left();
			}
			else if (!leaf && (user_input[0] == 'N' || user_input[0] == 'n'))
			{
				current = current->get_right();
			}
		}

		if (leaf && (user_input[0] == 'N' || user_input[0] == 'n'))
		{
			string newanimal, oldanimal, question;
			oldanimal = current->data();

			keep_going = false;
			cin.ignore(100,'\n');
			cout << "\nI give up. What are you?" << endl;
			getline(cin, newanimal, '\n');

			cout << "Please type a question that will distinguish a " << newanimal << " from a " << oldanimal << "." << endl;
			cout << "Enter a question: ";
			getline(cin, question, '\n');
			

			cout << "As a " << newanimal << ", " << question << endl;
			cout << "A: ";
			cin >> user_input;
			
			current->set_data(question);

			while (user_input[0] != 'Y' && user_input[0] != 'N' && user_input[0] != 'y' && user_input[0] != 'n')
			{
				cout << "Unknown Input! Please try again" << endl;
				cout << "As a " << newanimal << ", " << question << endl;
				cin.ignore();
				cout << "A: ";
				cin >> user_input;
			}

			if (user_input[0] == 'Y' || user_input[0] == 'y')
			{
				tree.add_left(question, newanimal, oldanimal);
				current->set_left(shared_ptr<Binary_tree_Node<string>>(new Binary_tree_Node<string>(newanimal)));
				current->set_right(shared_ptr<Binary_tree_Node<string>>(new Binary_tree_Node<string>(oldanimal)));
			}
			else
			{
				tree.add_right(question, oldanimal, newanimal);
				current->set_right(shared_ptr<Binary_tree_Node<string>>(new Binary_tree_Node<string>(newanimal)));
				current->set_left(shared_ptr<Binary_tree_Node<string>>(new Binary_tree_Node<string>(oldanimal)));
			}
		}
	}//End of one guessing game.
}


int main(int argc, char** argv)
{
	Binary_tree<string> tree(FILE_NAME);

	cout << "\nLet's Play a guessing game!" << endl;
	cout << "Pretend you are an animal and the program will ask you certain question to narrow down which animal you are." << endl;
	cout << "In reply you just enter \"Yes\" or \"No\". So, let's begin!" << endl << endl;

	string user_input;
	shared_ptr<Binary_tree_Node<string>> current;
	bool play_again = true;

	while (play_again)
	{
		current = tree.get_root();
		
		play_guess_game(tree, current); //Playing guess game.
		
		bool quit = false; //asking if user would like to play another.
		do
		{
			cout << "\nWould you like to play again." << endl;
			cout << "Enter \"Yes\" or \"No\"." << endl;
			cout << "Choice: ";
			cin >> user_input;

			if (user_input[0] == 'N' || user_input[0] == 'n')
			{
				play_again = false;
				quit = true;
			}
			else if (user_input[0] != 'Y' || user_input[0] != 'y')
			{
				play_again = true;
				quit = true;
			}
			else
			{
				cout << "\n\nUnknown Input, Please enter correctly." << endl << endl;
			}
		} while (!quit); //End of DO-WHILE, asking if user would like to play again.

	} //End of DO-WHILE, playing a game.

	system("Pause");
	return 0;
}
