/*
Author:		Gaurav Patel
Date:		July 10, 2015
Purpose:	Assignment 2 - Binary_tree class - Creates a specified type of binary tree when it's created to hold 
			information in tree structure manner, uses file to create a tree and writes to file when object gets 
			destroyed to maintain this program to use updated data each time.
*/

#pragma once

#include <iostream>
#include <vector>
#include <queue>
#include <memory>
#include <fstream>
using namespace std;

#include "BinaryTreeNode.h"

template <typename Item>
class Binary_tree
{
	string File_Name;
	vector<Item> arr_data;
	shared_ptr<Binary_tree_Node<Item>> root;
	shared_ptr<Binary_tree_Node<Item>> current;
	int total;

public:
	Binary_tree(string fname)
	{
		File_Name = fname;
		total = 0;
		root = nullptr;

		ifstream filestream(fname);
		string line;

		while (getline(filestream, line))
		{
			arr_data.push_back(line);
		}

		if (arr_data.size() > 0)
		{
			make_heap();
		}
	}

	~Binary_tree()
	{		
		//Used smart pointers so it will delete them self off when program is finished.
		ofstream fileoutput(File_Name);

		int sizeOfList = arr_data.size();
		
		if (fileoutput.is_open())
		{
			for (int i = 0; i < sizeOfList; i++)
			{
				fileoutput << arr_data[i] << endl;
			}

			fileoutput.close();
		}
		else
		{
			cout << File_Name << " file could not be opened, additional game data will not be added." << endl;
		}
	}

	void create_first_node(const Item& entry)
	{
		if (total == 0)
		{
			root = new shared_ptr<Binary_tree_Node<Item>>();
			root->set_data(entry);
			++total;
		}
	}
	
	void change(const Item& new_entry)
	{
		if (total > 0)
		{
			root->set_data(new_entry);
		}
	}

	void add_left(const Item& entry)
	{
		if (current->has_left_child() == false && total > 0)
		{
			shared_ptr<Binary_tree_Node<Item>> newNode = new shared_ptr<Binary_tree_Node<Item>>();
			newNode->set_data(entry);

			current->set_left(newNode);
			++total;
		}
	}

	void add_right(const Item& entry)
	{
		if (current->has_right_child() == false && total > 0)
		{
			shared_ptr<Binary_tree_Node<Item>> newNode = new shared_ptr<Binary_tree_Node<Item>>();
			newNode->set_data(entry);

			current->set_right(newNode);
			++total;
		}
	}

	size_t size() const
	{
		return total;
	}

	Item retrieve() const
	{
		return current->data();
	}

	bool has_parent() const
	{
		if (current != root)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool has_left_child() const
	{
		shared_ptr<Binary_tree<Item>> temp = current->get_left();
		if (temp != nullptr)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool has_right_child() const
	{
		shared_ptr<Binary_tree<Item>> temp = current->get_right();
		if (temp != nullptr)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void make_heap()
	{
		int total_data = arr_data.size();
		vector<shared_ptr<Binary_tree_Node<Item>>> ptr_arr(total_data, shared_ptr<Binary_tree_Node<Item>>());

		for (int i = 0; i < total_data; i++)
		{
			Item data = arr_data[i];
			
			if (data != "blank")
			{ 
				ptr_arr[i] = shared_ptr<Binary_tree_Node<Item>>(new Binary_tree_Node<Item>());
				ptr_arr[i]->set_data(data);
			}
		}

		root = current = ptr_arr[0];

		int r, l;
		for (int i = 0; i < total_data; i++)
		{
			r = 2 * i + 2;
			l = 2 * i + 1;

			if (r < total_data)
			{
				shared_ptr<Binary_tree_Node<Item>> right = ptr_arr[r];
				ptr_arr[i]->set_right(right);
			}
			else
			{
				i = total_data;
			}
			
			if (l < total_data)
			{
				shared_ptr<Binary_tree_Node<Item>> left = ptr_arr[l];
				ptr_arr[i]->set_left(left);
			}
			else
			{
				i = total_data;
			}
		}
	}

	shared_ptr<Binary_tree_Node<Item>> get_root()
	{
		return root;
	}

	void add_left(Item question, Item newanimal, Item oldanimal)
	{
		int total_data = arr_data.size();
		int index = total_data;

		for (int i = 0; i < total_data; i++)
		{
			if (arr_data[i] == oldanimal)
			{
				index = i;
				i = total_data;
			}
		}

		int r = 2 * index + 2;

		if (r > total_data)
		{
			while (total_data < r+1)
			{
				arr_data.push_back((Item)"blank");
				++total_data;
			}
		}

		arr_data[index] = question;
		arr_data[r - 1] = newanimal;
		arr_data[r] = oldanimal;
	}

	void add_right(Item question, Item oldanimal, Item newanimal)
	{
		int total_data = arr_data.size();
		int index = total_data;

		for (int i = 0; i < total_data; i++)
		{
			if (arr_data[i] == oldanimal)
			{
				index = i;
				i = total_data;
			}
		}

		int r = 2 * index + 2;

		if (r > total_data)
		{
			while (total_data < r+1)
			{
				arr_data.push_back((Item)"blank");
				++total_data;
			}
		}

		arr_data[index] = question;
		arr_data[r - 1] = oldanimal;
		arr_data[r] = newanimal;
	}
};